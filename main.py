
from functions import m_json
from functions import m_pck

path_kapazitaet = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
path_newton = "/home/pi/calorimetry_home/datasheets/setup_newton.json"

m_pck.check_sensors()

metadata = m_json.get_metadata_from_setup(path)
# in path soll man entweder path_kapazitaet oder path_newton schreiben
# path hängt von Versuch ab.

# serials in metadata addieren :
folder_path = "/home/pi/calorimetry_home/datasheets" # folder path aller dateien, ist von Versuch unabhängig.
metadata_serials = m_json.add_temperature_sensor_serials(folder_path,metadata)
 
# Messvorgang :
data = m_pck.get_meas_data_calorimetry(metadata_serials)
m_pck.logging_calorimetry(data,metadata_serials,"/home/pi/calorimetry_home/FOLDER-NAME",folder_path)
m_json.archiv_json(folder_path,path,"/home/pi/calorimetry_home/FOLDER-NAME")
 
 # !! Wo FOLDER-NAME steht muss geändert werden. Folder Name ist der Name wo die Versuchsdateien gespeichert werden !!
